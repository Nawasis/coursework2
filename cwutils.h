#ifndef COURSEWORK_H
#define COURSEWORK_H

//defining the class book for containing the information of each book
class book
{
 private:
  std::string title;
  std::string author;
  std::string ISBN;
  int qty;
  book *next;
 public:
  void settitle(std::string);
  void setauthor(std::string);
  void setISBN(std::string);
  void setqty(int);
  void setnext(book*);
  std::string gettitle();
  std::string getauthor();
  std::string getISBN();
  int getqty();
  book* getnext();
};

const int size = 500;
//function to retrive book data from a file
int retrivedata(std::string);
//hash function to calcuate hash value for a book object based on the title of the book
int send_to_hashtable(std::string);
//function to add a new book object to the table
void add_to_hash(int,book*);
//function to search for a any book
void search();
//function to add a book to the database
void addbook();
//to report a lost of damaged books
void lost_damaged();
//function to remove any book from the database
void remove(std::string);
//function to update the file after any changes that may have been made by the user
void update_file();
#endif

