cw : cwmain.o cwutils.o
	g++ cwmain.o cwutils.o -o cw

cwmain.o : cwmain.cpp
	 g++ -c cwmain.cpp

cwutils.o : cwutils.cpp
	  g++ -c cwutils.cpp

clean :
	rm *.o
	rm *.exe
