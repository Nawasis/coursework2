#include <iostream>
#include <string>
#include <ctype.h>
#include "cwutils.h"


book* database[size];
std::string filename = "";




int main(int argc, char* argv[])
{
  //if no filename if provided
  if(argc == 1)
    {
      std::cout<<"\n\nno filename provided exiting.... ";
      return 0;
    }
  //if more than required arguments are provided
  else if(argc > 2)
    {
      std::cout<<"\n Too many arguments provided. Plese provied a valid file name";
      return 0;
    }
  else if(argc == 2)
    {
      filename = argv[1];
    }

  
  int choice;
  char ch = 'y';
  //setting all the database[] to NULL and later the objects will be stored in these positions based on the index calculated by the hash function
  for(int i=0; i<size; i++)
    {
      database[i] = NULL;
    }
  //a function call to populate the database from the .txt file
  int g = retrivedata(filename);
  if(g == 0)
    {
      std::cout<<"\n\nInvalid file name provided. Could not find "<<argv[1];
      return 0;
    }

  //proving menu options untill user wants to use the system 
  while(ch == 'y' || ch == 'Y')
    {
      std::cout<<"\n\n|  MENU OPTIONS  | \n";
      std::cout<<"=================\n";
      std::cout<<"\n1. Search Book (by title)";
      std::cout<<"\n2. Add New Book";
      std::cout<<"\n3. Report Lost or damaged Books";
      std::cout<<"\n4. Exit";
      std::cout<<"\n\n Your Choice :\t";
      std::cin.clear();
      std::cin>>choice;
      if(std::cin.fail())
	{
	  std::cout<<"\n\nNot a valid choice. Exiting program...";
	  return 0;
	}
      if(choice == 1)
	{
	  std::cout<<"\n\nSEARCHING ...\n\n";
	  search();
	}
      else if(choice == 2)
	{
	  std::cout<<"\n\nADD NEW BOOK ...\n\n";
	  addbook();
	}
      else if(choice == 3)
	{
	  std::cout<<"\n\n REMOVING BOOK ...\n\n";
	  lost_damaged();
	}
      else if(choice == 4)
	{
	  std::cout<<"\n\nExiting ...";
	}
      else
	{
	  std::cout<<"\nWrong input. Please try again ...";
	}

      std::cout<<"\n\n\nTo continue..(press y or Y)";
      std::cin.clear();
      std::cin>>ch;
    }
  return 0;
}

