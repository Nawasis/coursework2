#include<iostream>
#include <fstream>
#include<sstream>
#include<string>
#include "cwutils.h"

extern book* database[size];
extern std::string filename;


//defining getter and setter functions for the node class define in "cwutils.h" file
void book::settitle(std::string x)
{
  this->title = x;
}

void book::setauthor(std::string x)
{
  this->author = x;
}

void book::setISBN(std::string x)
{
  this->ISBN = x;
}

void book::setqty(int x)
{
  this->qty = x;
}

void book::setnext(book* x)
{
  this->next = x;
}

std::string book::gettitle()
{
  return this->title;
}

std::string book::getauthor()
{
  return this->author;
}

std::string book::getISBN()
{
  return this->ISBN;
}

int book::getqty()
{
  return this->qty;
}

book* book::getnext()
{
  return this->next;
}

//a function to calculate the hash

int send_to_hashtable(std::string name)
{
  int j=0,x =0;
  // The hash value is calculated by adding the ASCII value of the each character inluding spaces and puntuation and modulus with size of array
  for(int i=0; i<name.length(); i++)
    {
      x = x + (int)name.at(i);
    }
  j =  x % size;
  return j;
}
    

//a function to retrive the data from the file using fstream objects
int retrivedata(std::string file1)
{
  //file opened in read only mode
  std::ifstream in;
  in.open(file1.c_str());
  
  std::string line;
  //check for error while opening file
  if (in.fail())
    {
      std::cout<<"could not open file";
      return 0;
    }
  //reading one line at a time from the file and processing it.
  while(getline(in,line))
    {
      int i=0;
      int j=0;
      int len = line.length();
      book *obj = new book;
      std::istringstream stm;
      std::size_t pos =0;
      std::string token;
      std::string delimeter = "\t";
      std::string qt;
      //using substring and delimeter as \t splicing the string to retrive data from file
      while((pos = line.find(delimeter)) != std::string::npos)
	{
	  token = line.substr(0,pos);
	  if(i == 0)
	    {
	      obj->settitle(token);
	      i++;
	    }
	  else if(i == 1)
	    {
	      obj->setauthor(token);
	      i++;
	    }
	  else if(i == 2)
	    {
	      obj->setISBN(token);
	      i++;
	    }
	  else if(i == 3)
	  {
	    qt = token;
	    i++;
	   }
	  line.erase(0,pos + delimeter.length());
	}
      //using stringstream to get the quantity and coverting it to integer value from astring value
      stm.str(qt);
      stm >> j;
      //std::cout<<qt;
      std::cout<<j<<"\n";
      //a check to only add book that has atleast 1 book in stock and ignoring book with with stock quantity below 1

	  obj->setqty(j);
	  obj->setnext(NULL);
	  //a function call that returns a array index after computing it in the hash function.
	  j = send_to_hashtable(obj->gettitle());
	  //based on the value return from the hash function the object is added to the hash table
	  add_to_hash(j,obj);
	  
    }

  return 1;
}


//a function to find appropriate place in the hashtable to add the new object considering collisions mmight happen and using channing

void add_to_hash(int pos, book* obj)
{
  //if the hash table is empty in the given obje will be stored in that particular position of the database 
  if(database[pos] == NULL)
    {
      database[pos] = obj;
    }
  //if collision happens then object needs to be stored as chaining as LL format
  else
    {
      if( database[pos]->getnext() == NULL)
	{
	  database[pos]->setnext(obj);
	}
      
      else {
	book *ptr = database[pos];
	//iterating to reach the end of the collision cause probing list
	while(ptr->getnext()!= NULL)
	  {
	    ptr = ptr->getnext();
	  }
	ptr->setnext(obj);
      }
    }
}

//a function to search any book based on the title of the book

void search()
{
  std::string str;
  std::cout<<"\nPlease enter the title of the book:\t";
  std::cin.ignore();
  std::getline(std::cin,str);
  //validating user input
  if(str == "")
    {
      std::cout<<"\n\nCannot search with an empty string. Aborting Procedure...\n";
      return;
    }
  //using a variable to store the index ofthe array computed usign the hash function
  int j = send_to_hashtable(str);
  if(database[j] == NULL)
    {
      std::cout<<"\n\n Item could not be found. Please try again with different query or confirm the title of the book(this search is case sensitive)";
      return;
    }
  book *ptr = database[j];

  //checking if the item is present in the table using the hash value as index
  if(database[j]->gettitle() == str)
    {
      std::cout<<"\n\nItem found.......\n\n";
      std::cout<<"Item details as follows ..";
      std::cout<<"\nTitle :\t"<<database[j]->gettitle();
      std::cout<<"\nAuthor :\t"<<database[j]->getauthor();
      std::cout<<"\nISBN :\t"<<database[j]->getISBN();
      std::cout<<"\nAvailable Stock :\t"<<database[j]->getqty();
    }
  //checking; if due to collision, have the item been stored as chaining method to the end of the probing list
  else
    {
      while(ptr->getnext() != NULL)
	{
	  if(ptr->gettitle() == str)
	    {
	      std::cout<<"\n\nItem found.......\n\n";
	      std::cout<<"Item details as follows ..";
	      std::cout<<"\nTitle :\t"<<ptr->gettitle();
	      std::cout<<"\nAuthor :\t"<<ptr->getauthor();
	      std::cout<<"\nISBN :\t"<<ptr->getISBN();
	      std::cout<<"\nAvailable Stock :\t"<<ptr->getqty();
	      return;
	    }
	  ptr = ptr->getnext();
	}
      //to check the last element of the collision list as the loop will break at the last element
      if(ptr->gettitle() == str)
	{
	  std::cout<<"\n\nItem found.......\n\n";
	  std::cout<<"Item details as follows ..";
	  std::cout<<"\nTitle :\t"<<ptr->gettitle();
	  std::cout<<"\nAuthor :\t"<<ptr->getauthor();
	  std::cout<<"\nISBN :\t"<<ptr->getISBN();
	  std::cout<<"\nAvailable Stock :\t"<<ptr->getqty();
	  return;	  
	}
      else
	{
	 std::cout<<"\n\n 2 Item could not be found. Please try again with different query or confirm the title of the book(this search is case sensitive)";
	}
    }
}
  

//a function to add new books
void addbook()
{
  std::string title,author,ISBN;
  int qty;
  std::cout<<"\nEnter the title of the book :\t";
  std::cin.ignore();
  std::getline(std::cin,title);
  std::cout<<"\nEnter the Author(s) of the book :\t";
  std::getline(std::cin,author);
  std::cout<<"\nEnter the ISBN number of the book :\t";
  std::getline(std::cin,ISBN);
  std::cout<<"\nEnter the number of copies of book to be added:\t";
  std::cin>>qty;
  if(std::cin.fail())
    {
      std::cout<<"\nInvalid stock entered. Aborting procedure ...";
      return;
    }
  //validating user inputs for the book details provided
  if(title == "" || author == "" || ISBN == "" )
    {
      std::cout<<"\n\nERROR..!!!";
      std::cout<<"\nBook details cannot be empty. Aborting procedure...";
      return;
    }
  else
    {
      //creating a new book object with the input taken from user
      book* ptr = new book;
      ptr->settitle(title);
      ptr->setauthor(author);
      ptr->setISBN(ISBN);
      ptr->setqty(qty);
      ptr->setnext(NULL);

      //calculating hash value
      int j = send_to_hashtable(ptr->gettitle());
      //add book to hashtable
      add_to_hash(j,ptr);
      std::cout<<"\nNew book added to database.";
    }
  update_file();
}


//Removing a book; reciving the title of the book
void remove(std::string title)
{
  //calculating the hashindex
  int j = send_to_hashtable(title);

  //check if the book is present on the database
  if( database[j] == NULL)
    {
       std::cout<<"\n The book you want to removed is not present in the database";
    }
  //if the book is present in the collision list then object is remove and the chain is relinked with previous object
  else
    {
      book *ptr = database[j]->getnext();
      book *ptr1 = database[j];
  
   if(database[j] == NULL)
    {
      std::cout<<"\nBook you are searching for is not present in the database";
      return;
    }
  if(database[j]->gettitle() == title)
    {
      database[j] = database[j]->getnext();
      std::cout<<"\n Booked removed from database";
      return;
    }
  else
    {
      while(ptr->getnext() != NULL)
	{
	  
	  if(ptr->gettitle() == title)
	    {
	      ptr1->setnext(ptr->getnext());
	      std::cout<<"\nBook removed from database";
	      return;
	    }
	  ptr1 = ptr1->getnext();
	  ptr = ptr->getnext();
	}
      if(ptr->gettitle() == title)
	{
	  ptr1->setnext(ptr->getnext());
	  std::cout<<"\nBook removed from database";
	  return;
	}
      std::cout<<"\n The book you want to removed is not present in the database";
    }
    }
  update_file();
}

void update_file()
{
  std::ofstream out;
  out.open(filename.c_str());

  for(int i = 0; i<size; i++)
    {
      if(database[i] != NULL)
	{
	  if(database[i]->getnext() == NULL )
	    {
	      out << database[i]->gettitle();
	      out << "\t";
	      out << database[i]->getauthor();
	      out << "\t";
	      out << database[i]->getISBN();
	      out << "\t";
	      out << database[i]->getqty();
	      out << "\t";
	      out << "\n";
	    }
	  else
	    {
	      book *ptr = database[i];
	      //while loop to traverse through the LL caused due to collisions
	      while(ptr->getnext() != NULL)
		{
		  //sending the data of the object to the file
		  out << ptr->gettitle();
		  out << "\t";
		  out << ptr->getauthor();
		  out << "\t";
		  out << ptr->getISBN();
		  out << "\t";
		  out << ptr->getqty();
		  out << "\t";
		  out << "\n";
		  ptr = ptr->getnext();
		}
	      //the last object in the linked list must also be stored in the file
	      out << ptr->gettitle();
	      out << "\t";
              out << ptr->getauthor();
              out << "\t";
	      out << ptr->getISBN();
	      out << "\t";
	      out << ptr->getqty();
	      out << "\t";
	      out << "\n";
	    }
	}
    }
  out.close();
}
	      
//function to report books as lost or stolen

void lost_damaged()
{
  std::cout<<"\nEnter the title of lost or stolen book :\t";
  std::string str;
  std::cin.ignore();
  std::getline(std::cin,str);
  std::cout<<"\nEnter how many books were lost or damaged:\t";
  int dn;
  std::cin>>dn;
  if(std::cin.fail())
    {
      std::cout<<"\n Invalid number of books entered. Aborting Procedure...";
    }
   if(str == "")
    {
      std::cout<<"\n\nCannot search with an empty string. Aborting Procedure...\n";
      return;
    }
  //using a variable to store the index ofthe array computed usign the hash function
  int j = send_to_hashtable(str);
  if(database[j] == NULL)
    {
      std::cout<<"\n\n Item could not be found. Please try again with different query or confirm the title of the book(this search is case sensitive)";
      return;
    }
  book *ptr = database[j];

  //checking if the item is present in the table using the hash value as index
  if(database[j]->gettitle() == str)
    {
      int q = database[j]->getqty();
      if( dn > q)
	{
	  std::cout<<"\n The number of affected cannot be more than the total Stock\nThe current stock is :\t"<<q;
	  std::cout<<"\n\nAborting Procedure ...";
	  return;
	}
      else if(dn < q)
	{
	  std::cout<<"Current stock is : "<<database[j]->getqty()<<std::endl;
	  database[j]->setqty(q - dn);
	  std::cout<<"After updation stock: " <<database[j]->getqty()<<std::endl;
	  std::cout<<"\n\nStock quantity Updated.";
	  update_file();
	  return;
	}
      else if(dn == q)
	{
	  std::cout<<"\nThe number of damaged book is the total item in Stock";
	  remove(str);
	  
	}

    }
  //checking; if due to collision, have the item been stored as chaining method
  else
    {
      while(ptr->getnext() != NULL)
	{
	  if(ptr->gettitle() == str)
	    {
	      int q = ptr->getqty();
	      if( dn > q)
		{
		  std::cout<<"\n The number of affected cannot be more than the total Stock\nThe current stock is :\t"<<q;
		  std::cout<<"\n\nAborting Procedure ...";
		  return;
		}
	      else if(dn < q)
		{
		  std::cout<<"Current stock is : "<<ptr->getqty()<<std::endl;
		  ptr->setqty(q-dn);
		  std::cout<<"After updation stock: " <<ptr->getqty()<<std::endl;
		  std::cout<<"\n\nStock quantity Updated.";
		  update_file();
		}
	      else if(dn == q)
		{
		  std::cout<<"\nThe number of damaged book is the total item in Stock";
		  remove(str);
		}
	      return;
	    }
	  ptr = ptr->getnext();
	}
      if(ptr->gettitle() == str)
	{
	  int q = ptr->getqty();
	      if( dn > q)
		{
		  std::cout<<"\n The number of affected cannot be more than the total Stock\nThe current stock is :\t"<<q;
		  std::cout<<"\n\nAborting Procedure ...";
		  return;
		}
	      else if(dn < q)
		{
		  std::cout<<"Current stock is : "<<ptr->getqty()<<std::endl;
		  ptr->setqty(q-dn);
		  std::cout<<"After updation stock: " <<ptr->getqty()<<std::endl;
		  std::cout<<"\n\nStock quantity Updated.";
		  update_file();
		}
	      else if(dn == q)
		{
		  std::cout<<"\nThe number of damaged book is the total item in Stock";
		  remove(str);
		}
	  return;	  
	}
      else
	{
	 std::cout<<"\n\n Item could not be found. Please try again with different query or confirm the title of the book(this search is case sensitive)";
	}
    }
  
}

